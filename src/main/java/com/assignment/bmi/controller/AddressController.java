package com.assignment.bmi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.bmi.exception.NotFoundException;
import com.assignment.bmi.model.Address;
import com.assignment.bmi.repository.AddressRepository;
import com.assignment.bmi.repository.EmployeeRespository;

@RestController
@RequestMapping("/api")
public class AddressController {

	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private EmployeeRespository employeeRespository;

	@GetMapping("/employee/{employeeId}/addresses")
	public List<Address> getContactByemployeeId(@PathVariable Long employeeId) {

		if(!employeeRespository.existsById(employeeId)) {
			throw new NotFoundException("Employee not found!");
		}

		return addressRepository.findByEmployeeId(employeeId);
	}

	@PostMapping("/employee/{employeeId}/addresses")
	public Address addAddress(@PathVariable Long employeeId,
			@Valid @RequestBody Address address) {
		return employeeRespository.findById(employeeId)
				.map(employee -> {
					address.setEmployee(employee);
					return addressRepository.save(address);
				}).orElseThrow(() -> new NotFoundException("Employee not found!"));
	}

	@PutMapping("/employee/{employeeId}/addresses/{addressId}")
	public Address updateAddress(@PathVariable Long employeeId,
			@PathVariable Long addressId,
			@Valid @RequestBody Address updateAddress) {

		if(!employeeRespository.existsById(employeeId)) {
			throw new NotFoundException("Employee not found!");
		}

		return addressRepository.findById(employeeId)
				.map(address -> {
					address.setAddressLine1(updateAddress.getAddressLine1());
					address.setAddressLine2(updateAddress.getAddressLine2());
					address.setAddressType(updateAddress.getAddressType());
					address.setCity(updateAddress.getCity());
					address.setCountry(updateAddress.getCountry());
					address.setState(updateAddress.getState());
					address.setZipCode(updateAddress.getZipCode());
					return addressRepository.save(address);
				}).orElseThrow(() -> new NotFoundException("Address not found!"));
	}

	@DeleteMapping("/employee/{employeeId}/addresses/{addressId}")
	public String deleteAddress(@PathVariable Long employeeId,
			@PathVariable Long addressId) {

		if(!employeeRespository.existsById(employeeId)) {
			throw new NotFoundException("Employee not found!");
		}

		return addressRepository.findById(addressId)
				.map(address -> {
					addressRepository.delete(address);
					return "Deleted Successfully!";
				}).orElseThrow(() -> new NotFoundException("Contact not found!"));
	}
}
