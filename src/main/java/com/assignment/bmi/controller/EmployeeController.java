package com.assignment.bmi.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.bmi.exception.NotFoundException;
import com.assignment.bmi.model.Employee;
import com.assignment.bmi.repository.EmployeeRespository;

@RestController
@RequestMapping("/api")
public class EmployeeController {

	@Autowired
	private EmployeeRespository employeeRespository;

	@GetMapping("/employees")
	public List<Employee> getAllEmployees() {
		return employeeRespository.findAll();
	}

	@GetMapping("/employee/{id}")
	public Employee getEmployeeByID(@PathVariable Long id) {
		Optional<Employee> employee = employeeRespository.findById(id);
		if(employee.isPresent()) {
			return employee.get();
		}
		else{
			throw new NotFoundException("Employee not found with id " + id);
		}
	}

	@PostMapping("/employee")
	public Employee createEmployee( @Valid @RequestBody Employee employee) {
		return employeeRespository.save(employee);
	}

	@PutMapping("/employee/{id}")
	public Employee updateEmployee(@PathVariable Long id,
			@Valid @RequestBody Employee updatedEmployee) {
		return employeeRespository.findById(id)
				.map(employee -> {
					employee.setFirstName(updatedEmployee.getFirstName());
					employee.setLastName(updatedEmployee.getLastName());
					employee.setGender(updatedEmployee.getGender());
					employee.setDob(updatedEmployee.getDob());
					employee.setDepartment(updatedEmployee.getDepartment());
					employee.setAge(updatedEmployee.getAge());
					return employeeRespository.save(employee);
				}).orElseThrow(() -> new NotFoundException("Employee not found with id " + id));
	}

	@DeleteMapping("/employee/{id}")
	public String deleteEmployee(@PathVariable Long id) {
		return employeeRespository.findById(id)
				.map(employee -> {
					employeeRespository.delete(employee);
					return "Delete Successfully!";
				}).orElseThrow(() -> new NotFoundException("Employee not found with id " + id));
	}
}
