package com.assignment.bmi.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.assignment.bmi.model.Employee;

public interface EmployeeRespository extends JpaRepository<Employee, Long>{
}
