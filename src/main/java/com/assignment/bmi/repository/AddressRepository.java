package com.assignment.bmi.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.assignment.bmi.model.Address;

public interface AddressRepository extends CrudRepository<Address, Long>{
	List<Address> findByEmployeeId(Long employeeId);	
}
